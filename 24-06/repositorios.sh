#!/bin/bash
#Mostramos las opciones del programa y con un read tomamos la opcion que 
#eliga el usuario. Utilizamos case para entrar en las diferentes opciones
#con rsync copiamos el contenido de los archivos que contienen los distintos
#repositorios a la ruta /etc/apt/sources.list. Y usamos diferentes comandos
#para instalar un .deb y resolver dependencias.

echo "OPCIONES: "
echo ""
echo "1.Habilitar repositorios Universe"
echo "2.Habilitar repositorios Multiverse"
echo "3.Habilitar repositorios Main"
echo "4.Habilitar todos los repositorios"
echo "5.Deshabilitar repositorios"
echo "6.Instalar .deb"
echo "7.Resolver dependencias"
echo "8.Salir"
echo ""
read -p "Ingrese el numero que corresponda: " opt

case $opt in
	1)rsync -va sourcesUniverse.list /etc/apt/sources.list
	echo ""
	echo "Repositorios agregados"
	;;
	2)rsync -va sourcesMultiverse.list /etc/apt/sources.list
	echo ""
        echo "Repositorios agregados"
	;;
	3)rsync -va sourcesMain.list /etc/apt/sources.list
	echo ""
        echo "Repositorios agregados"
	;;
	4)rsync -va sources.list /etc/apt/sources.list
	echo ""
        echo "Repositorios agregados"
	;;
	5)rsync -va sourcesVacio.list /etc/apt/sources.list
	echo ""
        echo "Repositorios eliminados"
	;;	
	6)read -p "Ingrese la ruta del paquete.deb a instalar: " ruta
	dpkg -i $ruta
	;;
	7)apt-get clean && apt-get autoclean 
	sleep 10
	apt-get update --fix-missing
	echo ""
        echo "Dependencias resueltas"
	;;
	8)echo ""
        echo "Salio del Script"
	exit 1
	;;
esac

#EOF
