#!/bin/bash

#Evaluamos la existencia de un fichero
#Usamos read para leer el fichero ingresado por el usuario
#guardamos en 2 variables los resultados de las busquedas
#la busqueda la realizamos con un find (decidi dejar solo la ruta
#home/istea ya que si ponemos el raiz tendriamos que ejecutar el script
#con sudo)
#-executable para buscar ficheros ejecutables y -user para buscar por usuario.
#Con test -n validamos que el largo del string no sea cero.

read -p "Indique el fichero con extensión: " fichero
info_fichero=$(find /home/istea -name $fichero -executable)
info_fichero2=$(find /home/istea -name $fichero -user $USER)

if test -n "$info_fichero"; then
	ls -l $info_fichero
	read -p "Desea ejecutarlo? Y/N: " ans
	if [[ $ans =~ [yY] ]]; then
		echo "Ejecutando fichero"
		exit
	fi
	echo "Saliendo"
	exit
elif test -n "$info_fichero2"; then
	read -p "No tiene permisos de ejecución desea otorgarlos Y/N: " ans1
	if [[ $ans1 =~ [yY] ]]; then
		chmod u+x $info_fichero2
		ls -l $info_fichero2
		exit
	fi
	echo "Saliendo"
else
	echo "no se encontro el fichero $fichero"
fi
#EOF
