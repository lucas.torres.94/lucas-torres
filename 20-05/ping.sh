#!/bin/bash

#Modificación del script para ejecutar un menu con instrucciones y no pasar
#los argumentos directos. Todos los valores son pasados a variables y usamos
#read para tomar los valores que se van ingresando. Luego pasamos todo al array.
read -p "Ingresar 'numero de pings': " ping
pings="-C $ping"
read -p "Ingresar protocolo '4' o '6': " pr
pro="-p $pr"
read -p "Ingresar -T 'timestamp' : " timesta
read -p "Ingresar -b: " opt
read -p "Ingresar ip o hostname: " ip
args=( $pings $pro $opt $timesta $ip )

#Con el for pasamos por el contenido del array y validamos con el case
#si coinicide realizara la validacion con if en caso de no ser favorable se saldra
#del script
for opcion in "${!args[@]}"; do
	
	case ${args[$opcion]} in
	#se valida que la cantidad de pings este dentro del rango 1 a 9
	-C) cantidad=${args[$(($opcion+1))]}
	if [[ $cantidad =~ [1-9] ]]; then
		counter="-c $cantidad"
	else
		echo "Cantidad de pings incorrecto, ingrese un numero entero positivo"
		exit
	fi	
	;;
	-T) timestamp="-D"
	;;
	#se valida que el protocolo sea 4 o 6 sino se sale del script
	-p) proto=${args[$(($opcion+1))]}
	if [[ $proto =~ [46] ]]; then
		p="-$proto"
	else
		echo "Protocolo incorrecto, deberia ser 4 o 6"
		exit
	fi	
	;;
	-b) b="-b"
	;;
	esac
done
#Validado lo anterior se verifica la ip o el hostname segun corresponda
#en el caso de la ip digitos dentro del rango 0 a 9 y de 1 a 3 numeros como
#maximo delante del punto. Si es por la url lo hacemos validando solo letras y
#puntos
if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
	ping $b $timestamp $p $counter $ip
elif [[ $ip =~ ^[a-z,.]{1,50}$ ]]; then
	ping $b $timestamp $p $counter $ip
else
	echo "Host/ip erroneo"
	exit
fi
