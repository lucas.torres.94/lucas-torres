#!bin/bash
#Ejecutamos el programa pasandole los argumentos correspondientes. 
#Validamos que las variables de origen y destino no esten vacias
#si pasa la validación, validamos que se haya solicitado o no realizar
#una llave y luego con el comando rsync realizamos la copia.
#con el ssh-keygent -t rsa generamos la llave para acceso remoto.
#También validamos la hora y si es correcto utilizamos el doble >> y pasamos
#la info para realizar una tarea programada.

org=$1
dest=$2
hor=$3
llave=$4

if [[ ! -z "$org" ]] && [[ ! -z "$dest" ]]; then
		if [[ "$llave" =~ "llave" ]]; then
			ssh-keygen -t rsa
			sudo ssh-copy-id -i /home/istea/.ssh/id_rsa.pub $dest
			rsync -va $org $dest
		else
			rsync -va $org $dest
		fi
		if [[ $hor -ge 0  ]] && [[ $hor -le 23 ]] ; then
			echo "* $hor * * * rsync -va $org $dest" >> /var/spool/cron/crontabs/istea
			echo ""
			echo "Se automatizara este backup"
		else
			echo ""
			echo "Hora no ingresada o valor incorrecto"
			echo "No se automatizara este backup"
		fi
else
	echo "Faltan valores de origen y/o destino"
	echo ""
	echo "Debe ejecutar el script con los argumentos en el siguiente orden: "
	echo "origen destino hora llave"
	echo ""
	echo "origen: es la ubicación a backupear"
	echo "destino: es la ubicación en donde se copiar el backup"
	echo "hora: es para automatizar la tarea en determinado horario(opcional)"
	echo "llave: es para generar llave de encriptación(opcional)"
fi
#EOF
