#!bin/bash
#Mostramos las opciones del programa y con un read tomamos la opcion que 
#eliga el usuario. Utilizamos case para entrar en las diferentes opciones
#pedimo al usuario que ingrese las rutas de origen y destino y se la pasamos
#al comando rsync para que realice la copia.
#con el ssh-keygent -t rsa generamos la llave para acceso remoto.
#utilizando el doble >> pasamos la info para realizar una tarea programada.

echo "OPCIONES: "
echo ""
echo "1.Backup local/remoto"
echo "2.Generar llaves"
echo "3.Automatizar Backup"
echo "4.Salir"
echo ""
read -p "Ingrese el numero que corresponda: " opt

case $opt in
	1)read -p "Ingrese direcctorio de origen: " org
	read -p "Ingrese drecctorio de destino: " dest
	rsync -va $org $dest
	;;
	2)ssh-keygen -t rsa
	adduser back
	sudo ssh-copy-id -i /home/istea/.ssh/id_rsa.pub back@localhost
	;;
	3)read -p "Ingrese direcctorio de origen: " org
        read -p "Ingrese drecctorio de destino: " dest
	read -p "ingrese hora a la que desea ejecutar el backup: " hor
	echo "* $hor * * * rsync -va $org $dest" >> /var/spool/cron/crontabs/istea
	;;
	4)echo ""
	echo "Salio del Script"
	exit
	;;
esac

#EOF
