#bin/bash
#Funcion para validar yes o no. retunr 0 si es igual a "Y o y" sino return 1
validar_yn () {
        if [[ $1 =~ [yY] ]]; then
                return 0
        else
                return 1
        fi
}

#Pasamos al array la ip ingresada separando los valores en diferentes indices
#esto se consigue cambiando el valor de la variable de entornoIFS por un "." 
#en vez de un espacio. Luego recorremos los indices con un for y validamos 
#con un if que sean digitos dentro del rango 0 a 9 y de 1 a 3 numeros como
#maximo. Si cumple return 0 sino return 1.
validar_ip () {

IFS_backup=$IFS
IFS="."

ip_arr=( $1 )
IFS=$IFS_backup

for opcion in "${!ip_arr[@]}"; do

        [[ ${ip_arr[$opcion]} =~ ^[0-9]{1,3}$ ]] || return 1

        if ! [ ${ip_arr[$opcion]} -le 255 ]; then
                return 1
        fi
done
return 0
}

#Si es por el hostname lo hacemos validando letras, puntos, guiones y
#numero si cumple return 0 sino return 1
validar_hostname () {
if [[ $1 =~ [a-zA-Z0-9_\-\.]+$ ]]; then
                return 0
        else
                return 1
        fi
}

#se valida que el contenido de la variable sea 4 o 6 si cumple return 0 sino 1.
validar_protocolo () {
if [[ $1 -eq 4 || $1 -eq 6 ]]; then
                return 0
        else
                return 1
        fi
}

#Se valida la cantidad de pings, que empiece por un numero entre 1 y 9 
#y que lo que los numeros que le sigan sea un numero entre 0 y 9
#Si cumple return 0 sino return 1.
validar_cantidad () {
	if [[ $1 =~ ^[1-9]&&[0-9]+ ]];then
		return 0
	else
       		return 1
	fi
}
#EOF
