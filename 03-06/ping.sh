source validaciones.sh

#!/bin/bash

#Modificación del script para ejecutar un menu con instrucciones y no pasar
#los argumentos directos. Todos los valores son pasados a variables y usamos
#read para tomar los valores que se van ingresando. Luego pasamos todo al array.

read -p "Ingresar 'numero de pings': " ping
pings="-C $ping"
read -p "Ingresar protocolo '4' o '6': " pr
pro="-p $pr"
read -p "Desea utilizar 'timestamp' presione y/n: " timesta

if validar_yn $timesta; then
	timesta="-T"
else
	echo "Continuara sin timestamp"
fi

read -p "Desea utilizar -b presione y/n: " opt

if validar_yn $opt; then
	opt="-b"
else
	echo "Continuara sin b"
fi

read -p "Desea ingresar una IP presione y/n: " ip

if validar_yn $ip; then
	read -p "ingresar IP: " ip
	if ! validar_ip $ip; then
		echo "IP invalido"
		exit
	fi
else
	read -p "Ingrese un hostname: " hostname
	if ! validar_hostname $hostname; then
		echo "Hostname invalido"
		exit
	fi
	ip=$hostname
fi

args=( $pings $pro $opt $timesta $ip )

#Con el for pasamos por el contenido del array y validamos con el case
#si coinicide realizara la validacion con if en caso de no ser favorable se
#saldra del script
for opcion in "${!args[@]}"; do
	
	case ${args[$opcion]} in
	#Se llama a la funcion "validar_catidad" y se le pasa un argument para  	que valide que la cantidad de pings que este dentro del rango 1 a 9
	-C) cantidad=${args[$(($opcion+1))]}
	if validar_cantidad $cantidad; then
		counter="-c $cantidad"
	else
		echo "Cantidad de pings incorrecto, ingrese un numero entero positivo"
		exit
	fi
	;;
	-T) timestamp="-D"
	;;
	#Se llama a la funcion para validar protocolo y se pasa un argumento.
	-p) proto=${args[$(($opcion+1))]}
	if validar_protocolo $proto; then
		p="-$proto"
	else
		echo "Protocolo incorrecto, deberia ser 4 o 6"
		exit
	fi	
	;;
	-b) b="-b"
	;;
	esac
done
#Validando lo anterior se verifica la ip o el hostname segun corresponda
#llamando a las funciones de validación correspondiente.

ping $b $timestamp $p $counter $ip
#EOF
